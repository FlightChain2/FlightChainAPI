import { EnvConfig } from './env';
import * as path from 'path';
import { ConfigOptions } from './config.model';

export const Appconfig: ConfigOptions = {
    hlf: {
        walletPath: EnvConfig.HFC_STORE_PATH, //path.resolve(__dirname, `creds`),
        admin: {
            MspID: EnvConfig.MSPID
        },
        channelId: EnvConfig.CHANNEL,
        chaincodeId: 'flightchain',

        tlsOptions: {
            trustedRoots: [],
            verify: false
        },
        caName: 'ca.sita.aero'
    }
} as ConfigOptions;

