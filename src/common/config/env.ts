import 'dotenv/config';
import * as path from "path";
import {HlfConfig} from "../../core/chain-interface/hlfconfig";
import {Log} from "../utils/logging/log.service";

export interface ProcessEnv {
    [key: string]: string | undefined;
}

/**
 * node EnvConfig variables,
 * copy .env.example file, rename to .env
 *
 * @export
 * @class EnvConfig
 */
export class EnvConfig {


    static initialise () {

//        console.log(process.env);

        // list env keys in console
        Log.config.debug('Initialising the environment variables.');

        for (let propName of Object.keys(EnvConfig)) {
            Log.config.debug(`${propName}:  ${EnvConfig[propName]}`);
        }
    }
    // NODE
    public static LISTENING_PORT = process.env['LISTENING_PORT'] || 3000;
    public static NODE_ENV = process.env['NODE_ENV'] || 'LOCAL';

    // FABRIC
    public static IDENTITY = process.env['IDENTITY'];
    public static HFC_STORE_PATH = process.env['HFC_STORE_PATH'] || path.join('./', 'bootstrap/hfc-key-store');
    public static CHANNEL = process.env['CHANNEL'] || 'channel-flight-chain';

    // Organisational MSP Id
    public static MSPID: string = process.env['MSPID'] || 'SITAMSP';

    // True if this app is in demo mode, and not connected to a fabric network.
    public static IS_DEMO_MODE = process.env['IS_DEMO_MODE'] || false;


    public static USE_SITA_TEST_FABRIC_NETWORK = process.env['USE_SITA_TEST_FABRIC_NETWORK'] || false;

    public static USER_TOKENS = process.env['USER_TOKENS'];

}