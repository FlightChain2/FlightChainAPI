import {AcrisFlight} from "../../acris-schema/AcrisFlight";


export class InvalidFlightData implements Error {
    constructor(msg: string, flight: AcrisFlight) {
        this.message = msg;
    }
    flight: AcrisFlight
    message: string;
    name: string;
}
export class UniqueKeyException implements Error {
    constructor(e: Error) {
        this.message = e.message;
        this.name = e.name;
    }

    message: string;
    name: string;
}

export class ACRISFlightValidation {


    /**
     * Validate the ACRIS json data.
     *
     * @param {AcrisFlight} flight
     * @throws Error InvalidFlightData exception if the ACRIS is not valid
     */
    static verifyValidACRIS(flight: AcrisFlight): void {

        if (!flight || !flight.operatingAirline || !flight.operatingAirline.iataCode || flight.operatingAirline.iataCode.length !== 2) {
            const msg = `Invalid flight data, there is no valid flight.operatingAirline.iataCode set.`;
            console.log(msg, flight);
            throw new InvalidFlightData(msg, flight);
        }
        if (!flight || !flight.departureAirport || flight.departureAirport.length !== 3) {
            const msg = 'Invalid flight data, there is no valid flight.departureAirport set.';
            console.log(msg, flight);
            throw new InvalidFlightData(msg, flight);
        }
        if (!flight || !flight.arrivalAirport || flight.arrivalAirport.length !== 3) {
            const msg = 'Invalid flight data, there is no valid flight.arrivalAirport set.';
            console.log(msg, flight);
            throw new InvalidFlightData(msg, flight);
        }
        if (!flight || !flight.flightNumber || !flight.flightNumber.trackNumber || flight.flightNumber.trackNumber.length !== 4) {
            const msg = 'Invalid flight data, there is no valid 4 digit flight.flightNumber.trackNumber set.';
            console.log(msg, flight);
            throw new InvalidFlightData(msg, flight);
        }
        if (!flight || !flight.originDate || !Date.parse(flight.originDate)) {
            const msg = 'Invalid flight data, there is no valid flight.originDate set (e.g. 2018-09-13).';
            console.log(msg, flight);
            throw new InvalidFlightData(msg, flight);
        }
    }


    /**
     * Generate the unique key for a given acris flight.
     * @param flight
     * @throws Exception if the flight data is missing mandatory fields.
     */
    static generateUniqueKey(flight: AcrisFlight): string {

        try {
            let flightNum = flight.flightNumber.trackNumber;
            while (flightNum.length < 4)
                flightNum = '0' + flightNum;
            return flight.originDate + flight.departureAirport + flight.operatingAirline.iataCode + flightNum;
        } catch (e) {
            console.log(`generateUniqueKey Exception ${e.message}, json data: `, flight);
            throw new UniqueKeyException(e);
        }
    }
}

