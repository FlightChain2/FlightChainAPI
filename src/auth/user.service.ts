import {Injectable} from "@nestjs/common";
import {EnvConfig} from "../common/config/env";

interface UserToken {
    user:string;
    token: string;
}

@Injectable()
export class UserService {

    private users:UserToken[] = null;
    /**
     * The user tokens are set via a environment variable that is injected from a k8s config map
     *
     * In a true production system, this would be loaded from a DB or an API management service.
     */
    constructor() {
        this.users = JSON.parse(EnvConfig.USER_TOKENS);
        console.log('UserService - initialised tokens for the following users:');
        this.users.forEach(user => {
            console.log(user.user);
        });
    }

    findOneByToken(token: string): boolean {
        let found = false;
        this.users.forEach((user:UserToken) => {
            if (user.token === token)
                found = true;
        });
        return found;
    }
}