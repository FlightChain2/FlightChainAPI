import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { HttpStrategy } from './http.strategy';
import {UserService} from "./user.service";
// import { UsersModule } from '../users/users.module';

@Module({
    // imports: [UsersModule],
    providers: [AuthService, HttpStrategy, UserService]
})
export class AuthModule {}