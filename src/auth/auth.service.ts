import { Injectable } from '@nestjs/common';
import {UserService} from "./user.service";

@Injectable()
export class AuthService {
    constructor(private readonly userService: UserService) {}


    async validateUser(token: string): Promise<any> {
        // Validate if token passed along with HTTP request
        // is associated with any registered account in the database
        console.log(`validateUser - token: ${token.substring(0, token.length/4)}...`, );
        //return new Promise<any>(resolve => {resolve('OK-USER')})
        return await this.userService.findOneByToken(token);
    }
}