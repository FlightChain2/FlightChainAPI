# flight-chain-api

## Description

REST API Interface to Flight Chain chaincode

The API interface exposes the chaincode. This is a CRU (as opposed to a CRUD) interface. There is no delete on a
blockchain so the operations are Create, Read, Update.
 

## Installation

Run this to install all the node_modules.

```bash
$ npm install
```

## Register admin/users

For local testing, it is necessary to bootstrap the network with an admin user
and some airline/airport users. The certificates & pem file are stored in `bootstrap/hfc-key-store` by default

> **NOTE** You must run this script every time you rebuild your blockchain network.
This script will create an admin users, and register users for BA, MIA, GVA & LHR.

```
export CA_ENDPOINT=http://localhost:7054
export MSPID=SITAMSP
./setupUsers.sh
```


## Registering users on k8s environment

### Setup the user accounts.

Use the k8s dashboard to exec into one of the `cli` pods. 

    git clone https://gitlab.com/FlightChain2/FlightChainAPI.git
    npm i
    export MSPID=$CORE_PEER_LOCALMSPID
    export CA_ENDPOINT=https://ca-sitaeu.fabric12:7054
    # The FABRIC_CA_SERVER_CA_NAME value is the FABRIC_CA_SERVER_CA_NAME env variable on the CA pod.
    export FABRIC_CA_SERVER_CA_NAME=ca 
    
This script file will create accounts on the Fabric CA server -  FlightChainBA, FlightChainMIA, FlightChainLHR
These files are stored in ./bootstrap/hfc-key-store 

    ./setupUsers.sh


### Configuring the  

    cat bootstrap/hfc-key-store/FlightChainBA

TODO: Update docs with how to transfer this data to the FLightChainAPI CI variables for deployment to the k8s pods.


 

## Running the app (for local network testing)

When you start the app, you must specify via environment variables which port the instance of the REST API is listening 
on and what airline or airport identity is related to this instance.

There are some convenience scripts in package.json to set these environment variables.  

```bash

# launch in watch mode for British Airways
$ npm run start:ba-local


```

## User Interface

View the swagger docs for the API on `http://localhost:<LISTEN_PORT>/docs`


## API Security

API Security is currently enforced by the k8s ingress rule. There is a HTTP basic
user/password check on incoming API calls. This may have to evolve as it is
deployed in environments outside of the sandbox k8s infrastructure.


## Tests

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

